# vim: set ft=make 
set dotenv-load := true

default: watch

fmt:
  @echo "Formatting Rust..."
  find {{invocation_directory()}} -name \*.rs -exec rustfmt --edition 2021 {} \; 

watch:
  @echo "Watching..."
  RUST_LOG=trace cargo watch -x run
  
run:
  @echo "Running..."
  RUST_LOG=trace cargo run

build:
  cargo build

open browser="w3m" path="http://localhost:8420":
  {{browser}} {{path}}
