use crate::akkoma::{types, AkkomaClient};
use axum::{
    extract::State,
    response::{Html, IntoResponse, Redirect},
    routing::{get, post},
    Form, Router,
};
use once_cell::sync::OnceCell;
use std::net::SocketAddr;
use tower_http::trace::TraceLayer;

mod akkoma;
mod downsampler;
mod images;
mod templates;
mod timeline;

static AKKOMA: OnceCell<AkkomaClient> = OnceCell::new();

#[derive(Clone)]
pub struct AppContext {
    pub akkoma: AkkomaClient,
    pub metadata: types::NodeinfoMetadata,
    pub tmp_dir: std::path::PathBuf,
}

#[tokio::main]
async fn main() {
    tracing_subscriber::fmt()
        .with_env_filter(
            tracing_subscriber::EnvFilter::from_default_env()
                .add_directive("tower_http=trace".parse().unwrap()),
        )
        .init();

    let backend_url = std::env::var("BACKEND_URL").expect("BACKEND_URL is missing.");
    let akkoma_client = AkkomaClient::new(backend_url.clone());
    let nodeinfo = akkoma_client
        .get_nodeinfo()
        .await
        .expect(format!("nodeinfo call failed; BACKEND_URL={}", backend_url).as_str());

    let app_context = AppContext {
        akkoma: akkoma_client,
        metadata: nodeinfo.metadata,
        tmp_dir: std::env::temp_dir(),
    };

    let app = Router::new()
        .route("/login", get(login))
        .route("/login/post", post(login_post))
        .route("/timeline/:timeline_type", get(timeline::timeline))
        .route("/", get(root))
        .layer(TraceLayer::new_for_http())
        .with_state(app_context);

    let addr = SocketAddr::from((
        [0, 0, 0, 0],
        std::env::var("PORT")
            .unwrap_or("8420".to_string())
            .parse()
            .unwrap(),
    ));

    tracing::debug!("listening on http://{}", addr.to_string());

    let listener = tokio::net::TcpListener::bind(&addr).await.unwrap();
    axum::serve(listener, app).await.unwrap();
}

async fn root(State(ctx): State<AppContext>) -> Html<String> {
    let data = templates::Index {
        global: templates::Global {
            instance_name: ctx.metadata.node_name,
        },
    };

    let out = templates::index(data).expect("failed to render");

    Html(out)
}

async fn login() -> Html<String> {
    let data = templates::Login {
        global: templates::Global {
            instance_name: "sapphic.engineer".to_string(),
        },
        csrf: templates::Csrf {
            token: nanoid::nanoid!(36),
        },
    };

    let out = templates::login(data).expect("failed to render");

    Html(out)
}

#[derive(serde::Deserialize, Debug)]
struct LoginForm {
    username: String,
    password: String,
    otp: Option<String>,
    csrf: Option<String>,
}

async fn login_post(Form(form): Form<LoginForm>) -> impl IntoResponse {
    tracing::debug!("login => {:?}", form);
    Redirect::to("/timeline/friends")
}

struct TimelineParams {
    pub timeline_type: String,
}

//async fn timeline(State(ctx): State<AppContext>) -> Html<String> {
//  let data = templates::Timeline {
//  global: templates::Global {
//  instance_name: ctx.metadata.node_name,
//  },
//  csrf: templates::Csrf {
//  token: nanoid::nanoid!(36),
//  },
//  timeline_type: params.timeline_type
//  };

// let out = templates::timeline(data).expect("failed to render");
// Html(out)
// }
