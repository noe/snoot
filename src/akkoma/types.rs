use serde::{Deserialize, Serialize};
use std::{collections::HashMap};

#[derive(Serialize, Deserialize, Clone)]
pub struct Nodeinfo {
    pub metadata: NodeinfoMetadata,
}

#[derive(Serialize, Deserialize, Clone)]
pub struct NodeinfoMetadata {
    #[serde(rename = "nodeName")]
    pub node_name: String,
    #[serde(rename = "nodeDescription")]
    pub node_description: String,
    pub features: Vec<String>,
}

#[derive(Serialize, Deserialize, Clone)]
pub struct Message {
    pub id: String,
    pub visibility: String,

    pub created_at: String,
    pub edited_at: Option<String>,

    pub content: String,
    pub spoiler_text: String,
    pub pleroma: PleromaMessage,

    pub account: Account,

    pub replies_count: u32,
    pub favourites_count: u32,
    pub reblogs_count: u32,

    pub favourited: bool,
    pub reblogged: bool,
    pub bookmarked: bool,
    pub pinned: bool,
    pub sensitive: bool,
}

#[derive(Serialize, Deserialize, Clone)]
pub struct PleromaMessage {
    pub content: Option<HashMap<String, String>>,
}

//impl fmt::Display for PleromaMessage {
//    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
//        match &self.content {
//            Some(content) => match content.get("text/plain") {
//                Some(text) => write!(f, "{}", text),
//                None => write!(f, "{:?}", content),
//            },
//            None => write!(f, "null"),
//        }
//    }
//}

#[derive(Serialize, Deserialize, Clone)]
pub struct Account {
  pub id: String,
  pub avatar: String,
  pub fqn: String,
  pub display_name: String,
  pub url: String,

  // additive
  pub avatar_html: Option<String>,
}
