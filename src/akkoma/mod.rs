use reqwest::get;

pub mod types;

#[derive(Clone)]
pub struct AkkomaClient {
    pub backend_url: String,
    pub nodeinfo: types::Nodeinfo,
}

impl AkkomaClient {
    pub fn new(backend_url: String) -> Self {
        AkkomaClient {
            backend_url,
            nodeinfo: types::Nodeinfo {
                metadata: types::NodeinfoMetadata {
                    node_name: "snoot.warme.st".to_string(),
                    node_description: "WARM KITTEN SNOOTS".to_string(),
                    features: vec!["akkoma_api".to_string()],
                },
            },
        }
    }

    pub async fn get_nodeinfo(&self) -> Result<types::Nodeinfo, reqwest::Error> {
        let nodeinfo = get(format!("{}/nodeinfo/2.1.json", self.backend_url))
            .await?
            .json::<types::Nodeinfo>()
            .await?;

        Ok(nodeinfo)
    }

    pub async fn get_timeline(&self, which: String) -> Result<Vec<types::Message>, reqwest::Error> {
        let timeline = get(format!("{}/api/v1/timelines/{}", self.backend_url, which))
            .await?
            .json::<Vec<types::Message>>()
            .await?;

        Ok(timeline)
    }
}
