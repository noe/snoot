use axum::{
  response::Html, extract::{State, Path,},
};
use crate::{templates, images::load_image};

pub async fn timeline(Path(timeline_type): Path<String>, State(ctx): State<crate::AppContext>) -> Html<String> {
  let mut timeline = match ctx.akkoma.get_timeline(timeline_type.clone()).await {
    Ok(timeline) => timeline,
    Err(e) => {
      tracing::error!("timeline -> fetch failed: {:?}", e);
      return Html(String::from("fetch failed, see logs"));
    },
  };

  for activity in &mut timeline {
    activity.account.avatar_html = Some(load_image(activity.account.avatar.clone(), 10).await.expect("timeline -> load_image failed"));
  }

  let data = templates::Timeline {
    global: templates::Global {
      instance_name: ctx.metadata.node_name,
    },
    csrf: templates::Csrf {
      token: nanoid::nanoid!(),
    },
    timeline_type,
    timeline,
  };

  Html(templates::timeline(data).expect("timeline -> template render failed")) 
}
