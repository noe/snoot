use crate::downsampler;

async fn render_image_from_url(url: String, size: u8) -> Result<String, String> {
    let resp = match reqwest::get(url).await {
        Ok(resp) => match resp.bytes().await {
            Ok(resp) => resp,
            Err(e) => {
                return Err(format!(
                    "render_image_from_url -> failed to retrieve bytes: {:?}",
                    e
                ))
            }
        },
        Err(e) => return Err(format!("render_image_from_url -> request failed: {:?}", e)),
    };

    let img = match image::load_from_memory(&resp) {
        Ok(img) => img.to_rgba8(),
        Err(e) => {
            return Err(format!(
                "render_image_from_url -> failed to load image: {:?}",
                e
            ))
        }
    };

    let intermediate = downsampler::sample_from_image(img, size, downsampler::FilterType::Nearest);
    let pixels = downsampler::sampled_image_to_matrix(intermediate, size);
    let html = downsampler::unicode_pixels_to_html(pixels);

    Ok(format!("{}", html))
}

pub async fn load_image(url: String, size: u8) -> Result<String, String> {
    const SEED: u64 = 0x04206969caffe143;
    let url_hash = xxh3::hash64_with_seed(url.clone().as_bytes(), SEED);
    let cache_filename = format!("snoot-{:x},{}.html.zs", url_hash, size);
    let cache_path = std::env::temp_dir().join(cache_filename);

    if cache_path.exists() {
      let cached_file = std::fs::File::open(cache_path.clone()).expect("load_image -> open failed");

      let read_buf = std::io::BufReader::new(cached_file);
      let mut content_buf = bytebuffer::ByteBuffer::new();
      match zstd::stream::copy_decode(read_buf, &mut content_buf) {
        Ok(_) => match content_buf.read_string() {
          Ok(content) => { 
            tracing::debug!("load_image -> cached {}", cache_path.display());
            return Ok(content); 
          },
          Err(e) => { tracing::warn!("load_image -> failed to render buffer as string: {:?}", e); },
        },
        Err(e) => {
          tracing::warn!("load_image -> copy_decode failed: {:?}", e);
        },
      }
    }

    let rendered = match render_image_from_url(url, size).await {
      Ok(rendered) => rendered,
      Err(s) => return Err(s),
    };

    let cache_file = std::fs::File::create(cache_path).expect("load_image -> create file failed");
    let mut rendered_buf = bytebuffer::ByteBuffer::new();
    rendered_buf.write_string(&rendered);
    if let Err(e) = zstd::stream::copy_encode(&mut rendered_buf, &cache_file, 0) {
      tracing::warn!("load_image -> cached file write failed: {:?}", e);
    }
    
    Ok(rendered)
}
