use serde::{Deserialize, Serialize};
use tera::{Context, Tera};
use crate::akkoma::types;

lazy_static::lazy_static! {
  pub static ref TEMPLATES: Tera = {
    let mut tera = Tera::new("src/templates/**/*").expect("templates failed to load");
    tera.autoescape_on(vec![".escaped.html"]);

    // tracing::debug!("TERA TEMPLATES => {:?}", tera);

    tera
  };
}

#[derive(Deserialize, Serialize, Debug)]
pub struct Global {
    pub instance_name: String,
}

#[derive(Deserialize, Serialize, Debug)]
pub struct Csrf {
    pub token: String,
}

#[derive(Deserialize, Serialize, Debug)]
pub struct Index {
    pub global: Global,
}

pub fn index(values: Index) -> Result<String, tera::Error> {
    let mut context = Context::from_serialize(values)?;

    Ok(TEMPLATES.render("index.html", &context)?)
}

#[derive(Deserialize, Serialize, Debug)]
pub struct Login {
    pub global: Global,
    pub csrf: Csrf,
}

pub fn login(values: Login) -> Result<String, tera::Error> {
    let mut context = Context::from_serialize(values)?;

    Ok(TEMPLATES.render("login.html", &context)?)
}

#[derive(Deserialize, Serialize)]
pub struct Timeline {
    pub global: Global,
    pub csrf: Csrf,
    pub timeline_type: String,
    pub timeline: Vec<types::Message>,
}

pub fn timeline(values: Timeline) -> Result<String, tera::Error> {
    let context = Context::from_serialize(values)?;
    Ok(TEMPLATES.render("timeline.html", &context)?)
}
