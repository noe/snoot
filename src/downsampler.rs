// Take an image, downsample it to block text in HTML

use image::{imageops, Pixel, RgbaImage};

pub fn create_blank(size: usize) -> Vec<String> {
    let blocks: usize = size * size;

    // We add "size" number of <br>
    let mut elements: Vec<String> = Vec::with_capacity(blocks + size);

    for y in 0..size / 2 {
        for x in 0..size {
            elements.push(format!(
                "<font color=\"#ff{}{}{}{}\">{}</font>",
                y,
                y,
                x,
                x,
                if y % 2 == 0 && x % 2 == 1 { "#" } else { "X" }
            ));
        }
        elements.push("<br />".to_string());
    }

    elements
}

pub use image::imageops::FilterType;

pub fn sample_from_image(img: RgbaImage, size: u8, filter: FilterType) -> RgbaImage {
    // This happens in two main steps and a correction
    // 1 -> Resize image to size x size
    // 2 -> Make a new alpha channel from greyscale
    // 3 ->-> Resize image to size x (size / 2) to fit blocks

    // Downscale to size x size
    let downscaled = imageops::thumbnail(&img, size.into(), size.into());

    // Do the greyscale; XXX: Does doing this against thumbnail cause problems?
    let greyscale = imageops::colorops::grayscale_alpha(&downscaled);

    // Merge our greyscale with our downscale
    let mut processed = downscaled.clone();
    for py in 0..size {
        for px in 0..size {
            let g_pixel = greyscale.get_pixel(px.into(), py.into()).to_luma_alpha();

            // If there was an alpha value before, it's likely 0 or 1,
            // so our modulus should be friendly to those values.
            let new_alpha =
                ((g_pixel[0] as f32 / 255.0) * (g_pixel[1] as f32 / 255.0) * 255.0 % 256.0) as u8;

            let mut c_pixel = processed.get_pixel(px.into(), py.into()).to_rgba();
            c_pixel[3] = new_alpha;

            processed.put_pixel(px.into(), py.into(), c_pixel);
        }
    }

    // Do our final resize down to size x (size/2)
    let output = imageops::resize(&processed, size.into(), (size / 2).into(), filter);

    output
}

pub struct UnicodePixel {
    char: &'static str,
    color: [u8; 3],
}

pub fn sampled_image_to_matrix(img: RgbaImage, size: u8) -> Vec<Vec<UnicodePixel>> {
    // Rows are half the height now
    let mut rows: Vec<Vec<UnicodePixel>> = Vec::with_capacity((size / 2).into());

    for py in 0..(size / 2) {
        let mut row: Vec<UnicodePixel> = Vec::with_capacity(size.into());

        for px in 0..size {
            let pixel = img.get_pixel(px.into(), py.into());

            let char = match pixel[3] {
                a if a <= 51 => " ",
                a if a <= 102 => "░",
                a if a <= 153 => "▒",
                a if a <= 204 => "▓",
                _ => "█",
            };

            row.push(UnicodePixel {
                char,
                color: [pixel[0], pixel[1], pixel[2]],
            });
        }

        rows.push(row);
    }

    rows
}

pub fn unicode_pixels_to_html(pixels: Vec<Vec<UnicodePixel>>) -> String {
    // Individual rows
    let mut row_elements = Vec::<String>::with_capacity(pixels.len());

    for py in 0..pixels.len() {
        let row = &pixels[py];

        let mut col_elements = Vec::<String>::with_capacity(row.len());

        for px in 0..row.len() {
            let pixel = &row[px];
            col_elements.push(format!(
                "<font color=\"rgb({},{},{})\">{}</font>",
                pixel.color[0], pixel.color[1], pixel.color[2], pixel.char
            ));
        }

        row_elements.push(col_elements.join(""));
    }

    row_elements.join("<br />")
}
