# snoot

a hyper-minimalist fediverse frontend, designed for use with akkoma and w3m/text-mode browsers

JS is strictly "additive", if ever. CSS is sometimes permissible.

## extreme warning

**Do not use this yet.** It has low to no security features, and is a PoC. You can literally be deleted from the internet/fedi if you aren't careful.
